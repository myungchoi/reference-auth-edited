package org.hspconsortium.platform.authorization.launchcontext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.hspconsortium.platform.security.LaunchContext;
import org.json.JSONObject;
import org.smartplatforms.oauth2.LaunchOrchestrationReceiveEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.firebase.internal.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

@Component
public class fhirEndpointLaunchContextHolder implements LaunchContextHolder {

	@Value("${hspc.platform.auth.contextHolder.url}")
	private String contextHolderUrl;

	@Value("${hspc.platform.auth.contextHolder.username}")
	private String contextHolderUsername;

	@Value("${hspc.platform.auth.contextHolder.password}")
	private String contextHolderPassword;

//	private Map<String, LaunchContext> contextMap = new HashMap<>();

	private LaunchContext constructLaunchContext(String jsonStr) {
		LaunchContext launchContext = new LaunchContext();
		
		// Convert the JSON string to JSON Object.
		JSONObject contextJson = new JSONObject(jsonStr);
		String launchId = contextJson.get("launch_id").toString();
		
		// Set values...
		launchContext.setLaunchId(launchId);
		
		// Add parameters
		JSONObject paramJson = contextJson.getJSONObject("parameters");
		for (String paramKey: paramJson.keySet()) {
			launchContext.addLaunchContextParam(paramKey, paramJson.get(paramKey));
		}
		
		return launchContext;
	}
	
	private String constructUrl(String launchContextId) {
		String url = contextHolderUrl;
		if (contextHolderUrl != null) {
			if (contextHolderUrl.endsWith("/") == false) {
				url = url + "/";
			}
			
			if (launchContextId != null) {
				url = url + launchContextId;
			}
		}
		
		return url;
	}
	
	@Override
	public LaunchContext getLaunchContext(String launchContextId) {
		// Talk to context holder to get the LaunchContext.
		CloseableHttpClient httpclient = HttpClients.createDefault();
		String url = constructUrl(launchContextId);
		
		HttpGet httpGet = new HttpGet(url);
		String unhashedString = contextHolderUsername + ":" + contextHolderPassword;
		String hashedString;

		CloseableHttpResponse httpResponse = null;
		try {
			hashedString = Base64.encodeToString(unhashedString.getBytes("UTF-8"), Base64.DEFAULT);
			httpGet.setHeader("Authorization", "Basic " + hashedString.substring(0, hashedString.length() - 1));
			httpGet.setHeader("Accept", "application/json");

			httpResponse = httpclient.execute(httpGet);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LaunchContext launchContext = null;
		if (httpResponse != null) {
			StatusLine statusLine = httpResponse.getStatusLine();
			if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
				String body = null;
				try {
					body = EntityUtils.toString(httpResponse.getEntity());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (body != null) {
					launchContext = constructLaunchContext(body);
				}
			}
		}
		return launchContext;
	}

	@Override
	public LaunchContext putLaunchContext(String contextId, LaunchContext launchContext) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		String url = constructUrl(null);
		HttpPost httpPost = new HttpPost(url);
		
		String unhashedString = contextHolderUsername + ":" + contextHolderPassword;
		String hashedString;
		String result = null;
		try {
			hashedString = Base64.encodeToString(unhashedString.getBytes("UTF-8"), Base64.DEFAULT);
			httpPost.setHeader("Authorization", "Basic " + hashedString.substring(0, hashedString.length() - 1));
			httpPost.setHeader("Accept", "application/json");

			// contextId will be replaced by the ContextHolder site. 
			String launchContextJson = launchContext.convertToJsonString();

			HttpEntity entity = new ByteArrayEntity(launchContextJson.getBytes("UTF-8"));
			httpPost.setEntity(entity);
					
			HttpResponse response = httpclient.execute(httpPost);
			result = EntityUtils.toString(response.getEntity());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (result != null) {
			launchContext = constructLaunchContext(result);
		}
		
		return launchContext;
	}

	@Override
	public LaunchContext removeLaunchContext(String launchContextId) {
		// The launchContext will be removed after a day. 
		// Just get the context and return it.
		return getLaunchContext(launchContextId);
	}
}
